
console.log(`utils.js init`)

export function row (content, styles = '') {
  return `<div class="row" style="${styles}">${content}</div>`
}

export function row_fluid (content, styles = '') {
  return `<div class="row-fluid" style="${styles}">${content}</div>`
}

export function col (content, classes = {}) {
  return `<div class="col-sm">${content}</div>`
}

export function css (styles = {}) {
  if (typeof styles === 'string') return styles

  const toString = key => `${key}: ${styles[key]}`
  return Object.keys(styles).map(toString).join(';')
}

export function block_form(type) {
  return `
<form id="${id}" name="${name}" style="${styles}">
  <h5>${type}</h5>
  <div class="form-group">
    <input id="" class="form-control form-control-sm" name="value" placeholder="value"/>
  </div>
  <div class="form-group">
    <input class="form-control form-control-sm" name="styles" placeholder="styles"/>
  </div>
  <button type="submit" class="btn btn-primary btn-sm">Добавить</button>
</form>
`
}
